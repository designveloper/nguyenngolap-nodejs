var express = require("express");
// A function that returns a middleware
var cors = require('cors');

var app = express();

var animeTerms = [
	{
		term: "Tsundere",
		defined: "Refers to girls that usually say the opposite of what they think"
	},
	{
		term: "Yandere",
		defined: "Refers to girls that can get insanely jealous"
	},
	{
		term: "Dandere",
		defined: "Refers to girls that are quiet, antisocial, but has a sweet, romantic and loving soft side"
	}
];

app.use(function(req, res, next) {
	console.log(`${req.method} request for '${req.url}'`);
	next();
});

app.use(express.static("./public"));

app.use(cors());

app.get("/dictionary-api", function(req, res) {
	res.json(animeTerms);
});

app.listen(3000);

console.log("Express app running on port 3000");

module.exports = app;
