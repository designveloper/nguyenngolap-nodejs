var express = require("express");
var cors = require("cors");
var bodyParser = require('body-parser');
var app = express();

var animeTerms = [
	{
		term: "Tsundere",
		defined: "Refers to girls that usually say the opposite of what they think"
	},
	{
		term: "Yandere",
		defined: "Refers to girls that can get insanely jealous"
	},
	{
		term: "Dandere",
		defined: "Refers to girls that are quiet, antisocial, but has a sweet, romantic and loving soft side"
	}
];

app.use(bodyParser.json());
// Set extended to true if there're large amounts of nested POST data to parse
app.use(bodyParser.urlencoded({ extended: false }));

app.use(function(req, res, next) {
	console.log(`${req.method} request for '${req.url}' - ${JSON.stringify(req.body)}`);
	next();
});

app.use(express.static("./public"));

app.use(cors());

app.get("/dictionary-api", function(req, res) {
	res.json(animeTerms);
});

app.post("/dictionary-api", function(req, res) {
    animeTerms.push(req.body);
    res.json(animeTerms);
});

app.delete("/dictionary-api/:term", function(req, res) {
    animeTerms = animeTerms.filter(function(definition) {
        return definition.term.toLowerCase() !== req.params.term.toLowerCase();
    });

    res.json(animeTerms);
})

app.listen(3000);

console.log("Express Mashiro on port 3000");

module.exports = app;
