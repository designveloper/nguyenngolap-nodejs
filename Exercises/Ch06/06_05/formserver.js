var http = require('http');
var fs = require('fs');

http.createServer(function(req, res) {
    console.log(`${req.method} for ${req.url}`);

    if (req.method === "GET") {
        res.writeHead(200, {"Content-Type": "text/html"});

        fs.createReadStream("./public/form.html", "UTF-8").pipe(res);
    } else if (req.method === "POST") {
        var body = "";

        req.on('data', function(chunk) {
            body += chunk;
        });

        req.on('end', function() {
            res.writeHead(200, {"Content-Type": "text/html"});

            res.end(`
                <!DOCTYPE html>
                <html>
                    <head>
                        <title>
                            Form Results
                        </title>
                    </head>
                    <body>
                        <h1>Your form result</h1>
                        <p>First name: ${body.first}</p>
                        <p>Last name: ${body.last}</p>
                        <p>Email: ${body.email}</p>
                    </body>
                </html>
            `)
        })
    }
}).listen(3000);

console.log("Mashiro's on port 3000!");
