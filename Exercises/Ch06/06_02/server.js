var http = require("http");

var server = http.createServer(function(req, res) {
    res.writeHead(200, {"Content-Type": "text/html"}); // Code 200: successful response

    res.end(`
        <DOCTYPE html>
        <html>
            <head>
                <title>HTML Response</title>
            </head>
            <body>
                <h1>Hello world!</h1>
                <p>${req.url}</p>
                <p>${req.method}</p>
            </body>
        </html>
    `);
});

server.listen(3000);
console.log("Mashiro's on port 3000!");
