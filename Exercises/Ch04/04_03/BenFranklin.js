var Person = require('./lib/Person');

// class Person extends EventEmitter {
//     constructor(name) {
//         super();
//         this.name = name;
//     }
//
//     said(saying) {
//         console.log(saying);
//     }
// }

var ben = new Person("Ben Franklin");
var ten = new Person("Ben Ten");

ben.on('speak', function(said) {
    console.log(`${this.name}: ${said}`);
});

ten.on('speak', function(said) {
    console.log(`${this.name}: ${said}`);
});

ben.emit('speak', "You bla bla...");
ten.emit('speak', "Ahahahaha");
