var https = require('https');

module.exports = {

	printName(person) {
		return `${person.last}, ${person.first}`;
	},

	loadWiki(name, callback) {
		var url = `https://en.wikipedia.org/wiki/${name}`;
		// Response is a stream of the downloaded page
		https.get(url, function(res) {
			var body = "";

			res.setEncoding("UTF-8");

			res.on('data', function(chunk) {
				body += chunk;
			})

			res.on('end', function() {
				callback(body);
			});
		});
	}

};
