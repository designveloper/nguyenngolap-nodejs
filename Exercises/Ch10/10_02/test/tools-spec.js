var expect = require("chai").expect;
var tools = require("../lib/tools");

describe("Tools", function() {
	describe("printName()", function() {
		it("should print the last name first", function() {
			var results = tools.printName({ first: "Alex", last: "Banks"});
			expect(results).to.equal("Banks, Alex");
		});
	});

	describe("loadWiki()", function() {
		// Mocha will wait 5 seconds for the callback function
		// to invoke before completing the test
		this.timeout(20000);

		// The "done" variable. Add it to tell Mocha that
		// it should wait for the function to invoke
		// before comleting the test
		it("load Giratina's wikipedia page", function(done) {
			tools.loadWiki("Giratina", function(html) {
				expect(html).to.be.ok;
				done();
			});
		});
	});
});
