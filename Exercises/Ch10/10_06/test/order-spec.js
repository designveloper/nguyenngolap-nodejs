var expect = require("chai").expect;
var rewire = require("rewire");

var order = rewire("../lib/order");

var sinon = require("sinon");

describe("Ordering Items", function() {

	beforeEach(function() {

		this.testData = [
			{sku: "AAA", qty: 10},
			{sku: "BBB", qty: 0},
			{sku: "CCC", qty: 3}
		];

		this.console = {
			log: sinon.spy()
		};

		this.warehouse = {
			// The yield function will invoke the callback function
			// that's sent to the packageAndShip function
			// In this case, the argument of that callback function
			// is "tracking", and we'll send a fake tracking number
			packageAndShip: sinon.stub().yields(28070405)
		}

		order.__set__("inventoryData", this.testData);
		order.__set__("console", this.console);
		order.__set__("warehouse", this.warehouse);

	});

	it("order an item when there are enough in stock", function(done) {

		var _this = this;

		order.orderItem("CCC", 3, function() {

			expect(_this.console.log.callCount).to.equal(2);

			done();
		});

	});

	describe("Warehouse interaction", function() {
		beforeEach(function() {
			this.callback = sinon.spy();

			order.orderItem("AAA", 2, this.callback);
		});

		it("receive tracking number", function() {
			expect(this.callback.calledWith(28070405)).to.equal(true);
		});

		it("calls packageAndShip with the correct sku and quantity", function() {
			expect(this.warehouse.packageAndShip.calledWith("AAA", 2)).to.equal(true);
		});
	});
});
