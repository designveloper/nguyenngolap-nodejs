var exports = {
    printName(person) {
        return `${person.last}, ${person.first}`;
    }
}

module.exports = exports;
