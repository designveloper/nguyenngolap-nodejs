var fs = require('fs');

// fs.renameSync("./assets", "./logs");
//
// console.log("Success!")

fs.readdirSync("./assets").forEach(function(fileName) {
    fs.unlinkSync("./assets/" + fileName);
});

fs.rmdir("./assets", function(err) {
    if (err) {
        throw err;
    }

    console.log("Success!");
});
