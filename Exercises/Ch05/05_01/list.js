var fs = require('fs');

// Read the directory synchronously
// Block the single thread.
// Every other request will have to wait for the synch request to finish
// var files = fs.readdirSync('./lib');

// Read the directory asynchronously
fs.readdir("./lib", function(err, files) {
    if (err) {
        throw err;
    }

    console.log(files);
});

console.log("Reading Files...");
