var fs = require('fs');

fs.unlink("notes.md", function(err) {
    if (err) {
        throw err;
    }

    console.log("notes.md removed!")
});

try {
    fs.unlinkSync("./lib/config.json");
    console.log("config.json removed!");
} catch (err) {
    throw err;
}
