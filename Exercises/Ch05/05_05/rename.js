var fs = require('fs');

fs.renameSync("./lib/project-config.js", "./lib/config.json");

fs.rename("./lib/notes.md", "./notes.md", function(err) {
    if (err) {
        throw err;
    }

    console.log("notes.md moved!");
});
