var fs = require('fs');

if (fs.existsSync("lib")) {
    console.log("Directory already existed!");
} else {
    fs.mkdirSync("lib", function(err) {
        if (err) {
            console.log(err);
        } else {
            console.log("Dir created!");
        }
    });
}
