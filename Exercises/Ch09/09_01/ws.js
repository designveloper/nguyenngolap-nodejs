var WebSocketServer = require("ws").Server;
var wss = new WebSocketServer({ port: 3000 });

// Every connection will invoke the callback function
wss.on('connection', function(ws) {
    ws.send("Welcome to Mashiro's exhibition!");
})
