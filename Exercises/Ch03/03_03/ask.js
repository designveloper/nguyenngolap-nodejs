var questions = [
    "What is your name?",
    "What are your hobbies?",
    "What is your favourite programming language?"
]

var answers = [];

function ask(i) {
    process.stdout.write(`${questions[i]}`);
    process.stdout.write(" > ");
}

process.stdin.on('data', function(data) {
    answers.push(data.toString().trim());
    if (answers.length < questions.length) {
        ask(answers.length);
    } else {
        process.exit();
    }
});

process.on("exit", function() {
    for (let i = 0; i < answers.length; i++) {
        process.stdout.write(`\n${questions[i]}\n`);
        process.stdout.write(`${answers[i]}\n`);
    }
});

ask(0);
