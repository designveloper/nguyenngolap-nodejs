# NodeJS
Intern: Nguyen Ngo Lap

## 1. What is NodeJS?
- Javascript
- History:
    - 2009: Node.js created
    - 2011: NPM created
    - 2014: io.js project forked (the users wanted a project governed by the open-source community)
    - 14/09/2015: Node.js 4.0 released

### The difference between Apache and Node.js
#### Apache

- Every request uses a thread
- Have to wait for the request to finish to make another (blocking)

#### Node.js

- Handles requests asynchronously (no blocking)
- All of the requests use the same thread (single-threaded)

## 3. Node core
\- `__dirname`: Path to the directory

\- `__filename`: Path to the file

\- `path` module: https://nodejs.org/api/path.html

\- The global object `process`:

- Provides information about, and control over, the current Node.js process
- References: https://nodejs.org/api/process.html#process_process

## 4. Node modules
\- **Core modules:** Modules installed locally with the installation of NodeJS

\- `events.EventEmitter`: Allows us to create listeners for an emit custom event

\- `module.exports`: An object that's returned by the `require` statement

\- `child_process.exec`: Allows executing external processes from other environments

\- `child_process.spawn`: Used to create child processes

- References: https://nodejs.org/api/child_process.html#child_process_child_process_spawn_command_args_options

## 5. The file system
\- `fs`: File system module

- Actions: read, write, create, append, rename, remove, ect.
- Can be sync or async, but should be async in most cases. Have to provide callback function if async
- References: https://nodejs.org/api/fs.html

\- The difference between logging error and throwing error:

- Throwing error causes the program to crash
- Logging error simply logs the error to the console, the program continues

### Streams

- An abstract interface for working with streaming data. E.g.: `process.stdin/out`
- Can be readable, writable, or both
- References:
    - https://nodejs.org/api/stream.html
    - The difference between readFile and readStream: https://stackoverflow.com/questions/38459334/what-is-difference-between-file-reading-and-streaming
    - The difference between writeFile and writeStream: https://stackoverflow.com/questions/8769071/different-between-fs-writefile-and-fs-writestream

## 6. The HTTP Module
Creates web servers, making requests, handling responses, ect.

\- Common (default) port:

- HTTP: 80
- HTTPS: 443

\- Making requests to secure server: https://nodejs.org/api/https.html#https_https_request_options_callback

\- `req` and `res` object:

- `req`: contains info about the requested headers, data going along with the request, info about user, etc.
- `res`: blank object, needs to be completed

\- `server.listen`: specifies the IP address and incoming port for the requests to the server

### API
**Application program interface**

Used to serve data to client applications. Any client that can make HTTP request can communicate with an API.

## 7. Node package manager
Npm page: https://www.npmjs.com/

- *To execute locally-installed plugins like `node-dev`, `httpster`, etc., have to reference their executable file directly from the node_modules folder*

## 8. Web servers
### Express framework
The most popular framework for developing web server applications.

\- **package.json**: A manifest that contains info about the app, and keeps track of the app's dependencies

\- Some used modules:

- `cors`: Cross Origin Resource Sharing - Makes the api accessible by other domains
- `body-parser`: parses the encoded data

\- **Middleware**: Customized plugins that adds functionality to the app. Use with `app.use()`.

- E.g.:
    - `express.static()` - static file server
    - `cors()`
    - Custom functions `app.use(function(...){...})`
- Each custom function has 3 argument: `req`, `res`, `next`. Have to invoke the `next` function at the end to tell the app to move on to the next middleware

## 9. Web sockets

- Allow for a true two way connection between clients and servers
- Send and receive messages from a TCP server
- References: https://www.tutorialspoint.com/html5/html5_websocket.htm

### Polling, Long Polling and Web Sockets

- **Polling**: Constantly making request to the server to check if the state of the server has changed
- **Long polling**: Making a request to the server and leaving it open for a longer time
- **Web sockets**:
    - Connecting to a socket server and leaving the connection open to send and receive data.
    - A two-way connection: Clients can send data to server -> Servers broadcast changes to every connection open

### Socket.IO
- Falls back to long polling
- Helps a web socket application work where web socket is not supported

## 10. Testing and Debugging
Jasmine, Mocha, Jest, QUnit, ...

\- Mocha: https://mochajs.org/

\- Chai: http://chaijs.com/

\- `--save-dev` flag: Save the things that are needed to work with the app

### Mocking a server with Nock
Creates a mock web server to test any function that requires hitting a web page.

- Use Mocha's hooks

### Injecting dependencies with Rewire
Injecting fake data to the test.

### Advanced Testing
#### Sinon spies

Functions that record how they're called, the arguments, etc.

Can be used in place of real functions to check if a specific function has been invoked.

##### Sinon stubs

More powerful spies - Allow the controlling of a particular function: return data, callback, throw error, etc.

### Code coverage with Istanbul
Generates code coverage report - Showing how many lines of code the tests cover.

*In Windows, have to provide the corrent path to `_mocha`.*

### Testing HTTP endpoints with Supertest
Tests the HTTP endpoints: The return of a HTML page, etc.

### Checking server responses with Cheerio
Allows searching the DOM just as jQuery.

## 11. Automation and deployment
\- **Grunt:** A command line interface, used to run automatic processes

\- **Browserify:** Allow the use of CommonJS for client-side JS. It will automatically package up all used modules and dependencies into one bundled file to send to the client.

\- **Watches:** Tasks run when change and save a file

\- `node-inspector`: Debugging tools

- (?) On windows, `node-inspector & node --debug app` doesn't work asynchronously, so the workaround are:
    - Use `start [/b]`: Start the specified application without waiting for the previous ones to complete
    - Split them into 2 mini-thread and run
    - ...
